<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use App\Post;
use Auth;

class CommentController extends Controller
{
    public function index($id)
    {

        $post = Post::where('id', $id)->with(['user'])->first();
        $comment = Comment::where('id_post', $id)->with(['user'])->get();
        // dd($comment);
        // dd($post);
        return view('readmore',compact('post', 'comment'));
    }

    public function input(Request $request, $id)
    {
        $validatedData = $request->validate([
            'comment' => 'required|max:255'
        ]);

        $data = new Comment();
        $data->comment = $request->comment;
        $data->id_user = Auth::user()->id;
        $data->id_post = $id;
        // dd($data);
        $data->save();
        return redirect()->back();
    }

    public function edit($id){
        $post = Post::where('id', $id)->first();
        return view('edit-post',compact('post') );
        // echo 'edit '. $id;
    }

    public function update(Request $request, $id){
        $post = Post::where('id', $id)->first();
        $post->title = $request->title;
        $post->content = $request->content;
        $post->save();
        // dd($post);

        return redirect()->route('comment',$id);
    }

    public function delete($id){
        $post = Post::where('id', $id)->first();
        $comment = Comment::where('id_post', $id)->get();

        $post->delete();
        foreach($comment as $u) {
            $u->delete();
        }

        return redirect()->back();

    }
}
