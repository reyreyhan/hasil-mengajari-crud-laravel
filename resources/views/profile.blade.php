@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Profile</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('home') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="nama" class="col-md-2 col-form-label"><strong>{{ __('Nama') }}</strong></label>
                            <label for="nama" class="col-form-label">{{ $data->name }}</label>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-2 col-form-label"><strong>{{ __('Email') }}</strong></label>
                            <label for="email" class="col-form-label">{{ $data->email }}</label>
                        </div>
                    </form>
                </div>
            </div><br>
            <div class="card">
                <div class="card-header">Edit Profile</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('profile.update', $data->id ) }}">
                        @csrf
                        @method('PUT')
                        <div class="form-group row">
                            <label for="nama" class="col-md-4 col-form-label text-md-right"><strong>{{ __('Nama') }}</strong></label>
                            <div class="col-md-6">
                                <input id="title" type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="name" value="{{ $data->name }}" required autofocus>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Edit Profile') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div><br>
            <div class="card">
                <div class="card-header">Posting</div>

                <div class="card-body">
                    @foreach($post as $post)
                    <div class="form-group row">
                        <label for="nama" class="col-md-12 col-form-label text-md-center"><strong>{{ $post->title }}</strong></label>
                    </div>
                    <div class="form-group row">
                        <label for="nama" class="col-md-12 col-form-label text-md-left">{{ $post->content  }}</label>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
