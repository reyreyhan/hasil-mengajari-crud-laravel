<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/home', 'HomeController@input')->name('home');

Route::post('/comment/{id}', 'CommentController@input')->name('comment');
Route::get('/comment/{id}', 'CommentController@index')->name('comment');
Route::get('/comment-edit/{id}', 'CommentController@edit')->name('comment-edit');
Route::put('/comment-edit/{id}', 'CommentController@update')->name('comment-edit');
Route::delete('/comment-delete/{id}', 'CommentController@delete')->name('comment-delete');
Route::resource('/profile', 'ProfileController');


// Route::get('/hasil',
//     function(){
//         return 'berhasil';
//     }
// )->name('hasil');
